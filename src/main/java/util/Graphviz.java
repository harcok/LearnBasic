package util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;
import org.zeroturnaround.exec.InvalidExitValueException;
import org.zeroturnaround.exec.ProcessExecutor;

import learner.Main;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.serialization.dot.GraphDOT;
//import net.automatalib.util.graphs.dot.GraphDOT;
import net.automatalib.words.Alphabet;

public class Graphviz {
	static final Logger logger = Logger.getLogger(Graphviz.class);

	public static boolean GENERATE_PDF_FROM_DOT = false;
	public static long DOT_CMD_TIMEOUT_IN_SECONDS = 10;

	public static void convertDot2Format(String basename, String format ) {
		String dotfile = new File(basename + ".dot").getAbsolutePath();
		String outfile = new File(basename + "." + format).getAbsolutePath();

		// see https://github.com/zeroturnaround/zt-exec
		ProcessExecutor cmd=new ProcessExecutor()
				                  .command("dot", "-T" + format, dotfile, "-o", outfile)
				                  .exitValueNormal()
				                  .timeout(DOT_CMD_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);
		try {
			cmd.execute();
		} catch (InvalidExitValueException | IOException | InterruptedException | TimeoutException e) {
			// none fatal error, just report and continue
			logger.error("problem in converting " +basename  + ".dot to " + format );
			logger.debug(e);
		}
	}

	/**
	 * Produces a dot-file and a PDF (if graphviz is installed)
	 * @param fileName filename without extension - will be used for the .dot and .pdf
	 * @param model
	 * @param alphabet
	 * @param verboseError whether to print an error explaing that you need graphviz
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void produceOutput(String fileName, MealyMachine<?,String,?,String> model, Alphabet<String> alphabet) throws FileNotFoundException, IOException {
		PrintWriter dotWriter = new PrintWriter(fileName + ".dot");
		GraphDOT.write(model, alphabet, dotWriter);
		dotWriter.close();

		if ( GENERATE_PDF_FROM_DOT ) convertDot2Format(fileName, "pdf" );

	}

}
