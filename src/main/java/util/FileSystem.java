package util;

import java.io.File;

public class FileSystem {

	/*
	 *  remove directory with all its contents,
	 *  where subdirectories are recursively removed
	 */
	static public void rmdirhier(String dir)
	{
	
	    File path = new File(dir);
	    _rmdir(path);
	}

	static protected void _rmdir(File path)
	{
	    if (path == null)
	        return;
	    if (path.exists())
	    {
	        for (File f : path.listFiles())
	        {
	            if (f.isDirectory())
	            {
	                _rmdir(f);
	                f.delete();
	            }
	            else
	            {
	                f.delete();
	            }
	        }
	        path.delete();
	    }
	}

	static public void mkdir(String dir) {
	    new File(dir).mkdir();
	}

	static public void recreateDir(String dir) {
		rmdirhier(dir);
		mkdir(dir);
	}

}
