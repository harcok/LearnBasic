package learner;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import de.learnlib.acex.analyzers.AcexAnalyzers;
import de.learnlib.datastructure.observationtable.OTUtils;
import de.learnlib.datastructure.observationtable.ObservationTable;
import de.learnlib.datastructure.observationtable.writer.ObservationTableASCIIWriter;
import de.learnlib.driver.util.MealySimulatorSUL;
import de.learnlib.algorithms.kv.mealy.KearnsVaziraniMealy;
import de.learnlib.algorithms.lstar.ce.ObservationTableCEXHandlers;
import de.learnlib.algorithms.lstar.closing.ClosingStrategies;
import de.learnlib.algorithms.lstar.mealy.ExtensibleLStarMealy;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy;
import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.algorithm.LearningAlgorithm;
import de.learnlib.api.oracle.MembershipOracle.MealyMembershipOracle;
import de.learnlib.api.SUL;
import de.learnlib.oracle.equivalence.WMethodEQOracle;
import de.learnlib.oracle.equivalence.WpMethodEQOracle;
import de.learnlib.oracle.equivalence.mealy.RandomWalkEQOracle;
import de.learnlib.util.Experiment.MealyExperiment;
import de.learnlib.api.logging.LearnLogger;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.filter.statistic.sul.ResetCounterSUL;
import de.learnlib.oracle.membership.SULOracle;
import de.learnlib.filter.statistic.sul.SymbolCounterSUL;
import de.learnlib.filter.statistic.Counter;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.automata.transout.impl.compact.CompactMealy;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;
import net.automatalib.words.impl.SimpleAlphabet;
import statemachine.model.fsm.mealy.file.Import;
import statemachine.model.fsm.mealy.MealyDetEnabledModel;
import statemachine.model.fsm.mealy.MealyModel;
import statemachine.model.fsm.mealy.MealyNonDetModel;
import statemachine.model.fsm.mealy.conversion.AutomataLib;
import sul.CoffeeMachine;
import sul.HardCodedExampleSUL;
import sul.SocketSUL;
import tester.YannakakisEQOracle;
import util.FileSystem;
import util.Graphviz;

/**
 * General learning testing framework. The most important parameters are the input alphabet and the SUL (The
 * first two static attributes). Other settings can also be configured.
 *
 * Based on the learner experiment setup of Joshua Moerman, https://gitlab.science.ru.nl/moerman/Learnlib-Experiments
 *
 * Setup in new form by Ramon Janssen
 *
 * further enhanced by Harco Kuppens
 */
public class Main {
	static final Logger logger = Logger.getLogger(Main.class);


	//*****************//
 	//  SUL to learn   //
	//*****************//
	// sul examples available
	public enum SULType { HardCodedExample, CoffeeFluentBuild, CoffeeModelBuild, CandyMachineOverSocket , RiverCrossingPuzzle, CoffeeModelFromDot}

	//set SUL to learn
	//private static final SULType sulType = SULType.HardCodedExample;  //hard coded  example
	//private static final SULType sulType = SULType.CoffeeFluentBuild;
	//private static final SULType sulType = SULType.CoffeeModelBuild;
	//private static final SULType sulType = SULType.CoffeeModelFromDot;
	private static final SULType sulType = SULType.RiverCrossingPuzzle;
	//private static final SULType sulType = SULType.CandyMachineOverSocket;  // learn CandyMachine over Socket



	//*******************************//
 	// General experiment settings   //
	//*******************************//

	// Experiments controlled by Learnlib produce very little feedback,
	// A self controlled experiment produces feedback after every hypothesis and is better suited to adjust by programming
	private static final boolean runSelfControlledExperiment = true;
	// For controlled experiments only: store every hypotheses as a file. Useful for 'debugging'
	// if the learner does not terminate (hint: the TTT-algorithm produces many hypotheses).
	private static final boolean saveAllHypotheses = true;

	static {
	 	// logging settings
		FileSystem.recreateDir("log/");  // throw away old logs
		String loggingConfigFile = "config/log4j.conf";
		PropertyConfigurator.configure(loggingConfigFile);

	 	// graphviz settings
		// only enable next when graphviz is installed and "dot" cmdline program in PATH
		Graphviz.GENERATE_PDF_FROM_DOT = false;
		Graphviz.DOT_CMD_TIMEOUT_IN_SECONDS = 10;
	}


	//*******************//
 	// Learning settings //
	//*******************//
	// file for writing the resulting .dot-file and .pdf-file (extensions are added automatically)
	private static final String FINAL_MODEL_FILENAME = "learnedModel",
								INTERMEDIATE_HYPOTHESIS_FILENAME = "log/hypothesis";

	// the learning algorithms. LStar is the basic algorithm, TTT performs much faster
	// but is a bit more inaccurate and produces more intermediate hypotheses, so test well)
	public enum LearningMethod { LStar, RivestSchapire, TTT, KearnsVazirani }
	private static  LearningMethod learningAlgorithm = LearningMethod.TTT; // default


	//*******************//
 	// Testing settings  //
	//*******************//

	// the testing algorithms
	public enum TestingMethod { RandomWalk, WMethod, WpMethod, LeeYannakakis }
	// Random walk is the simplest, but performs badly on large models: the chance of hitting a
	// erroneous long trace is very small
	private static  TestingMethod testMethod = TestingMethod.RandomWalk; //default


	// RandomWalk default settings
	// - the chance to do a reset after an input
	private static double chanceOfResetting = 0.1;  //default;  means 10 procent change of reset per input!
	// - the number of inputs to test before accepting a hypothesis
	private static int numberOfSymbols =  1000; //default; means 1000 inputs in total
	// so above default means:  approximately 100 queries of 10 symbols





	// LeeYannakakis requires a cmdline tool install
	static {
		// set cmdline tool location
		String yannakakisBinDir = "/usr/local/yannakakis/bin/";
		String yannakakisCmd = "main";
		YannakakisEQOracle.setYannakakisBinDir(yannakakisBinDir);
		YannakakisEQOracle.setYannakakisCmd(yannakakisCmd);
	}


	public static void main(String [] args) throws IOException {

		// Defines the input alphabet
		// Note you can even use other types than string, if you hange the generic-values.
		// e.g. make your SUL of type SUL<Integer, Float> for int-input and float-output
	    Alphabet<String> inputAlphabet;

		// Load the actual SUL-class, depending on which SUL-type is set at the top of this file
		// You can also program an own SUL-class if you extend SUL<String,String> (or SUL<S,T> in
		// general, with S and T the input and output types - you'll have to change some of the
		// code below)
		SUL<String,String> sul;

		// load SUL example with input alphabet and with learning and testing config
		switch (sulType) {

		case HardCodedExample:
			sul = new HardCodedExampleSUL();
			inputAlphabet = new SimpleAlphabet<String>(ImmutableSet.of("a", "b", "c"));

			learningAlgorithm = LearningMethod.TTT;
			testMethod = TestingMethod.RandomWalk;
			// different random walk params then default
			chanceOfResetting=0;
			numberOfSymbols =  100;

			//testMethod = TestingMethod.LeeYannakakis;

			break;

		case CoffeeFluentBuild:
			sul = CoffeeMachine.fluentBuildCoffeeMachineSUL();
			inputAlphabet= CoffeeMachine.getInputAlphabet();

			learningAlgorithm = LearningMethod.TTT;
			testMethod = TestingMethod.RandomWalk;

			break;

		case CoffeeModelBuild:
			sul = CoffeeMachine.modelBuildCoffeeMachineSUL();
			inputAlphabet= CoffeeMachine.getInputAlphabet();

			learningAlgorithm = LearningMethod.RivestSchapire;

			testMethod = TestingMethod.RandomWalk;
			break;
			
		case RiverCrossingPuzzle:	
            // read in RiverCrossingPuzzle model from dot file using statemachinelib
            // note: the general reader is used for all kind of mealy machines; so 
            //       it chooces the format with the most freedom
            MealyNonDetModel nondetmodel = Import.dot2MealyModel("RiverCrossingPuzzle/RiverCrossingPuzzle.dot");
            // verify model is deterministic and input enabled by converting it to that type of model
            MealyDetEnabledModel detmodel  = MealyDetEnabledModel.fromMealyModel(nondetmodel);
            // convert model in statemachinelib format to automatalib format
            CompactMealy<String, String> machine= AutomataLib.convert2automatalibMealy(detmodel) ;
             
            inputAlphabet=machine.getInputAlphabet();
            sul= new MealySimulatorSUL<>(machine);

            learningAlgorithm = LearningMethod.RivestSchapire;

            testMethod = TestingMethod.RandomWalk;
            break;			
            
        case CoffeeModelFromDot:
            // read in coffeemachine model from dot file using statemachinelib
            // note: the general reader is used for all kind of mealy machines; so 
            //       it chooces the format with the most freedom
            MealyNonDetModel xnondetmodel = Import.dot2MealyModel("CoffeeMachine/coffeemachine.dot");
            // verify model is deterministic and input enabled by converting it to that type of model
            MealyDetEnabledModel xdetmodel  = MealyDetEnabledModel.fromMealyModel(xnondetmodel);
            // convert model in statemachinelib format to automatalib format
            CompactMealy<String, String> xmachine= AutomataLib.convert2automatalibMealy(xdetmodel) ;
             
            inputAlphabet=xmachine.getInputAlphabet();
	        sul= new MealySimulatorSUL<>(xmachine);

            learningAlgorithm = LearningMethod.RivestSchapire;

            testMethod = TestingMethod.RandomWalk;
            break;

		case CandyMachineOverSocket:

			// IMPORTANT:
			//   This example is learning a remote CandyMachine over a socket connection.
			//   So before starting to learn start the remote CandyMachine in the
			//   project directory with the command :
			//      bash runCandyMachineSUL.sh

	        Alphabet<String> inputs = new SimpleAlphabet<>();
	        inputs.add("IBUTTONMARS");
	        inputs.add("IBUTTONSNICKERS");
	        inputs.add("ICOIN10");
	        inputs.add("ICOIN5");
	        inputs.add("IREFUND");
	        inputAlphabet=inputs;

			// for SUL the learner communicates with over a network socket
	    	// For SULs over socket, the socket address/port can be set here
	    	InetAddress socketIp = InetAddress.getLoopbackAddress();
	    	int socketPort = 7892;
	    	boolean printNewLineAfterEveryInput = true; // print newlines in the socket connection
	    	String resetCmd = "reset"; // the command to send over socket to reset sut
			sul = new SocketSUL(socketIp, socketPort, printNewLineAfterEveryInput, resetCmd);


			learningAlgorithm = LearningMethod.TTT;
			testMethod = TestingMethod.RandomWalk;

			break;

		default:
			throw new RuntimeException("No SUL-type defined");
		}


		// Wrap the SUL in counters for symbols/resets, so that we can record some statistics
		SymbolCounterSUL<String, String> symbolCounterSul = new SymbolCounterSUL<>("symbol counter", sul);
		ResetCounterSUL<String, String> resetCounterSul = new ResetCounterSUL<>("reset counter", symbolCounterSul);
		Counter nrSymbols = symbolCounterSul.getStatisticalData(), nrResets = resetCounterSul.getStatisticalData();
		// we should use the sul only through those wrappers
		sul = resetCounterSul;


		// log traces during learning which will be stored in log/memTraces.txt
		SUL<String,String> learning_sul = new LogTraces<>("memTraces",sul);
		MealyMembershipOracle<String,String> learning_sulOracle = new SULOracle<>(learning_sul);


		// ExtensibleLStarMealy<String, String>(Alphabet<String> alphabet, MembershipOracle<String, Word<String>> oracle, List<Word<String>> initialSuffixes, ObservationTableCEXHandler<? super String, ? super Word<String>> cexHandler, ClosingStrategy<? super String, ? super Word<String>> closingStrategy)


		// Choosing a learner
		LearningAlgorithm<MealyMachine<?, String, ?, String>, String, Word<String>> learner = null;
		//ExtensibleLStarMealy<MealyMachine<?, String, ?, String>, String, Word<String>> learner = null;
		//ExtensibleLStarMealy<String, String> xlearner = new ExtensibleLStarMealy<>(inputAlphabet, learning_sulOracle, Lists.<Word<String>>newArrayList(), ObservationTableCEXHandlers.RIVEST_SCHAPIRE, ClosingStrategies.CLOSE_SHORTEST);
		//LearningAlgorithm.MealyLearner<String, String>learner;

		switch (learningAlgorithm){
			case LStar:
				learner = new ExtensibleLStarMealy<>(inputAlphabet, learning_sulOracle, Lists.<Word<String>>newArrayList(), ObservationTableCEXHandlers.CLASSIC_LSTAR, ClosingStrategies.CLOSE_SHORTEST);
				break;
			case RivestSchapire:
			    List<Word<String>> initialSuffixes =Lists.newArrayList();
//			    for ( String input: inputAlphabet) {
//			        WordBuilder<String> builder=new WordBuilder<>();
//			        builder.add(input);
//			        initialSuffixes.add(builder.toWord());
//			    }

				learner = new ExtensibleLStarMealy<>(inputAlphabet, learning_sulOracle, initialSuffixes, ObservationTableCEXHandlers.RIVEST_SCHAPIRE, ClosingStrategies.CLOSE_SHORTEST);

				ObservationTable<String, Word<String>> table = ((ExtensibleLStarMealy<String,String>) learner ).getObservationTable();
				 ObservationTableASCIIWriter<String,Word<String>> writer=new ObservationTableASCIIWriter<>(true);
				 String tableStr=OTUtils.toString(table, writer);
				 logger.info("table:\n" +tableStr);
				break;
			case TTT:
				learner = new TTTLearnerMealy<>(inputAlphabet, learning_sulOracle, AcexAnalyzers.LINEAR_FWD);
				break;
			case KearnsVazirani:
				learner = new KearnsVaziraniMealy<>(inputAlphabet, learning_sulOracle, false, AcexAnalyzers.LINEAR_FWD);
				break;
			default:
				throw new RuntimeException("No learner selected");
		}


		// log traces during testing which will be stored in log/equivTraces.txt
		SUL<String,String> testing_sul = new LogTraces<>("equivTraces",sul);
		MealyMembershipOracle<String,String> testing_sulOracle = new SULOracle<>(testing_sul);

		// Choosing an equivalence oracle
		EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> eqOracle = null;
		EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> eqOracleFixed = null;
		switch (testMethod){
			// simplest method, but doesn't perform well in practice, especially for large models
			case RandomWalk:
				//eqOracle = new RandomWalkEQOracle<>(chanceOfResetting, numberOfSymbols, true, new Random(123456l), testing_sul);
                eqOracle = new RandomWalkEQOracle<String,String>(testing_sul, chanceOfResetting, numberOfSymbols,true  ,new Random(123456l));
                        //chanceOfResetting, numberOfSymbols, true, new Random(123456l), testing_sul);

				break;
			// Other methods are somewhat smarter than random testing: state coverage, trying to distinguish states, etc.
			case WMethod:
				//eqOracle = new WMethodEQOracle.MealyWMethodEQOracle<>(3, testing_sulOracle);
				eqOracle = new WMethodEQOracle.MealyWMethodEQOracle<>(testing_sulOracle,3);
				break;
			case WpMethod:
				//eqOracle = new WpMethodEQOracle.MealyWpMethodEQOracle<>(3, testing_sulOracle);
			    eqOracle = new WpMethodEQOracle.MealyWpMethodEQOracle<>(testing_sulOracle,3);
				break;
			case LeeYannakakis:
				// IMPORTANT: requires Yannakakis cmdline tool install
				// tool location
			    String yannakakisBinDir = "/usr/local/yannakakis/bin/";
				String yannakakisCmd = "main";
				YannakakisEQOracle.setYannakakisBinDir(yannakakisBinDir);
				YannakakisEQOracle.setYannakakisCmd(yannakakisCmd);

				// random infix tests
				int minimumNumberOfInfixSteps=8;
				int averageNumberOfInfixSteps=8;
				int seed = 7;
				long maximumNumberOfQueries = 200;
				eqOracle= new YannakakisEQOracle<>(testing_sulOracle, minimumNumberOfInfixSteps, averageNumberOfInfixSteps,maximumNumberOfQueries,seed);

				// fixed number of infix tests for maximum infix size
				int fixedNumberOfInfixSteps=2;
				eqOracleFixed = new YannakakisEQOracle<>(testing_sulOracle,fixedNumberOfInfixSteps);
                break;
			default:
				throw new RuntimeException("No test oracle selected!");
		}

		// Running the actual experiments!
		if (runSelfControlledExperiment) {
			runSelfControlledExperiment(learner, eqOracle, eqOracleFixed, nrSymbols, nrResets, inputAlphabet);
		} else {
			runLearnlibControlledExperiment(learner, eqOracle, inputAlphabet);
		}
	}


	/**
	 * Simple example of running a learning experiment
	 * @param learner Learning algorithm, wrapping the SUL
	 * @param eqOracle Testing algorithm, wrapping the SUL
	 * @param inputAlphabet Input alphabet
	 * @throws IOException if the result cannot be written
	 */
	public static void runLearnlibControlledExperiment(
			LearningAlgorithm<MealyMachine<?, String, ?, String>, String, Word<String>> learner,
			EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> eqOracle,
			Alphabet<String> inputAlphabet) throws IOException
	{

		// in experiment Learnlib uses LearnLogger, so configure it
		LearnLogger loggerLearnlib = LearnLogger.getLogger("de.learnlib");

		//TODO: => not available anymore in 13.1 : loggerLearnlib.setLevel(Level.ALL);

		//logger.info("java.util.logging.SimpleFormatter.format: " +  System.getProperty("java.util.logging.SimpleFormatter.format"));
		System.setProperty("java.util.logging.SimpleFormatter.format","[LEARNLIB_%4$s]: %5$s %n");


		logger.info("start learnlib controlled experiment");
		MealyExperiment<String, String> experiment = new MealyExperiment<String, String>(learner, eqOracle, inputAlphabet);
		experiment.run();
		logger.info("Ran " + experiment.getRounds().getCount() + " rounds");
		Graphviz.produceOutput(FINAL_MODEL_FILENAME, experiment.getFinalHypothesis(), inputAlphabet);
	}

	/**
	 * More detailed example of running a learning experiment. Starts learning, and then loops testing,
	 * and if counterexamples are found, refining again. Also prints some statistics about the experiment
	 * @param learner learner Learning algorithm, wrapping the SUL
	 * @param primaryEqOracle Testing algorithm, wrapping the SUL
	 * @param nrSymbols A counter for the number of symbols that have been sent to the SUL (for statistics)
	 * @param nrResets A counter for the number of resets that have been sent to the SUL (for statistics)
	 * @param inputAlphabet Input alphabet
	 * @throws IOException
	 */
	public static void runSelfControlledExperiment(
			LearningAlgorithm<MealyMachine<?, String, ?, String>, String, Word<String>> learner,
			EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> primaryEqOracle,
			EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> secondaryEqOracle,
			Counter nrSymbols, Counter nrResets,
			Alphabet<String> inputAlphabet) throws IOException {

		try {
			// prepare some counters for printing statistics
			int stage = 0;
			long lastNrResetsValue = 0, lastNrSymbolsValue = 0;

			// start the actual learning
			logger.info("===========================================================================");
			logger.info("START LEARNING PROCESS");
			logger.info("===========================================================================");
			logger.info("self controlled experiment");
			logger.info("learner: " + learningAlgorithm);
			logger.info("tester: " + testMethod);
			logger.info("===========================================================================");
			logger.info("BEGIN LEARNING");
			learner.startLearning();

			while(true) {

				logger.info("---------------------------------------------------------------------------");
				logger.info("FINISHED LEARNING");

                if ( learningAlgorithm == LearningMethod.RivestSchapire ) {
                    ObservationTable<String, Word<String>> table = ((ExtensibleLStarMealy<String,String>) learner ).getObservationTable();
                    ObservationTableASCIIWriter<String,Word<String>> writer=new ObservationTableASCIIWriter<>(true);

                    String tableStr=OTUtils.toString(table, writer);
                    logger.info("table:\n" +tableStr);
                }



				// store hypothesis as file
				if(saveAllHypotheses) {
					String outputFilename =  INTERMEDIATE_HYPOTHESIS_FILENAME + stage;
					Graphviz.produceOutput(outputFilename, learner.getHypothesisModel(), inputAlphabet);
					logger.info("model size" + learner.getHypothesisModel().getStates().size());
				}

				// Print statistics

				logger.info(stage + ": " + Calendar.getInstance().getTime());
				// Log number of queries/symbols
				logger.info("Hypothesis size: " + learner.getHypothesisModel().size() + " states");
				long roundResets = nrResets.getCount() - lastNrResetsValue, roundSymbols = nrSymbols.getCount() - lastNrSymbolsValue;
				logger.info("learning queries/symbols: " + nrResets.getCount() + "/" + nrSymbols.getCount()
						+ "(" + roundResets + "/" + roundSymbols + " this learning round)");

				//learner.getObservationTable();
				logger.info("---------------------------------------------------------------------------");
				logger.info("START TESTING");

				lastNrResetsValue = nrResets.getCount();
				lastNrSymbolsValue = nrSymbols.getCount();

				// Search for CE
				DefaultQuery<String, Word<String>> ce = primaryEqOracle.findCounterExample(learner.getHypothesisModel(), inputAlphabet);
				// retry with second eqOracle (secondaryEqOracle)
				if ( ce == null && secondaryEqOracle != null) {

					// Log number of queries/symbols
					roundResets = nrResets.getCount() - lastNrResetsValue;
					roundSymbols = nrSymbols.getCount() - lastNrSymbolsValue;

					logger.info("testing queries/symbols: " + nrResets.getCount() + "/" + nrSymbols.getCount()
					+ "(" + roundResets + "/" + roundSymbols + " this testing round)");


					logger.info("---------------------------------------------------------------------------");
					logger.info("FINISHED PRIMARY TESTING ALGORITM  => START SECONDARY TESTING ALGORITM");
					logger.info("---------------------------------------------------------------------------");


					lastNrResetsValue = nrResets.getCount();
					lastNrSymbolsValue = nrSymbols.getCount();

					ce = secondaryEqOracle.findCounterExample(learner.getHypothesisModel(), inputAlphabet);

				}

				logger.info("found COUNTEREXAMPLE on sut: " + ce);

				// Log number of queries/symbols
				roundResets = nrResets.getCount() - lastNrResetsValue;
				roundSymbols = nrSymbols.getCount() - lastNrSymbolsValue;

				logger.info("---------------------------------------------------------------------------");
				logger.info("FINISHED TESTING");

				logger.info("testing queries/symbols: " + nrResets.getCount() + "/" + nrSymbols.getCount()
						+ "(" + roundResets + "/" + roundSymbols + " this testing round)");

				logger.info("===========================================================================");


				lastNrResetsValue = nrResets.getCount();
				lastNrSymbolsValue = nrSymbols.getCount();

				if(ce == null) {
					// No counterexample found, stop learning
					logger.info("FINISHED LEARNING PROCESS");
					logger.info("===========================================================================");

					Graphviz.produceOutput(FINAL_MODEL_FILENAME, learner.getHypothesisModel(), inputAlphabet);
					break;
				} else {
					// Counterexample found, rinse and repeat
					stage++;

					logger.info("RESTART LEARNING with ce: "  + ce);  // prefix: Empty word ; suffix : inputs; output: outputs
					learner.refineHypothesis(ce);
				}

			}
		} catch (Exception e) {
			String errorHypName = "hyp.before.crash.dot";
			Graphviz.produceOutput(errorHypName, learner.getHypothesisModel(), inputAlphabet);
			throw e;
		}


	}

}
