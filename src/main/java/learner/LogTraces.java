package learner;
/**
 * author: Harco Kuppens
 */

import java.util.LinkedList;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import org.apache.log4j.Logger;

import de.learnlib.api.SUL;
//import de.learnlib.api.SULException;
import de.learnlib.api.exception.SULException;

@ParametersAreNonnullByDefault
public class LogTraces<I, O> implements SUL<I,O> {

	private  final Logger logger;
	private  final String logger_name;
	private final SUL<I,O> sul;

	private LinkedList<String> transitions;

	public LogTraces(String name, SUL<I,O> sul) {
		logger_name = name;
		logger = Logger.getLogger(logger_name);
		transitions=new LinkedList<String>();
		this.sul = sul;
	}



	/* (non-Javadoc)
	 * @see de.learnlib.api.SUL#pre()
	 */
	@Override
	public void pre() {
		sul.pre();
	}


	/* (non-Javadoc)
	 * @see de.learnlib.api.SUL#post()
	 */
        @Override
	public void post() {
		sul.post();

		logger.info(transitions);
		transitions.clear();

	}

	/* (non-Javadoc)
	 * @see de.learnlib.api.SUL#step(java.lang.Object)
	 */
	@Override
	@Nullable
	public O step(@Nullable I in) throws SULException {
		//logger.fatal(logger_name);
		O output= sul.step(in);
		transitions.add(in.toString() + "/" + output.toString() );
		return output;
	}



	@Override
	public boolean canFork() {
		return sul.canFork();
	}

	@Override
	public SUL<I,O> fork() {
		return new LogTraces<>(logger_name, sul.fork());
	}
}
