package sul;

import de.learnlib.api.SUL;
import de.learnlib.driver.util.MealySimulatorSUL;
import net.automatalib.automata.transout.impl.compact.CompactMealy;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.Alphabets;

public class CoffeeMachine {


	static public Alphabet<String> getInputAlphabet(){
		String WATER = "WATER";
		String POD = "POD";
		String BUTTON = "BUTTON";
		String CLEAN = "CLEAN";
		Alphabet<String> inputAlphabet = Alphabets.fromArray(WATER,POD,BUTTON,CLEAN);
		return inputAlphabet;
	}

	static public SUL<String,String> modelBuildCoffeeMachineSUL(){
		CompactMealy<String, String> machine = modelBuildCoffeeMachine();
		SUL<String,String> sul= new MealySimulatorSUL<>(machine);;
		return sul;
	 }

	static public SUL<String,String> fluentBuildCoffeeMachineSUL(){
		CompactMealy<String, String> machine = fluentBuildCoffeeMachine();
		SUL<String,String> sul= new MealySimulatorSUL<>(machine);;
		return sul;
	 }


	static public CompactMealy<String,String> modelBuildCoffeeMachine(){

		String WATER = "WATER";
		String POD = "POD";
		String BUTTON = "BUTTON";
		String CLEAN = "CLEAN";

		String out_ok = "ok";
		String out_error = "error";
		String out_coffee = "coffee";

		Alphabet<String> inputAlphabet = Alphabets.fromArray(WATER,POD,BUTTON,CLEAN);

		CompactMealy<String, String> machine = new CompactMealy<String,String>(inputAlphabet);

		Integer a = machine.addInitialState(), b = machine.addState(),
				c = machine.addState(), d = machine.addState(),
				e = machine.addState(), f = machine.addState();

		machine.addTransition(a, WATER, c, out_ok);
		machine.addTransition(a, POD, b, out_ok);
		machine.addTransition(a, BUTTON, f, out_error);
		machine.addTransition(a, CLEAN, a, out_ok);

		machine.addTransition(b, WATER, d, out_ok);
		machine.addTransition(b, POD, b, out_ok);
		machine.addTransition(b, BUTTON, f, out_error);
		machine.addTransition(b, CLEAN, a, out_ok);

		machine.addTransition(c, WATER, c, out_ok);
		machine.addTransition(c, POD, d, out_ok);
		machine.addTransition(c, BUTTON, f, out_error);
		machine.addTransition(c, CLEAN, a, out_ok);

		machine.addTransition(d, WATER, d, out_ok);
		machine.addTransition(d, POD, d, out_ok);
		machine.addTransition(d, BUTTON, e, out_coffee);
		machine.addTransition(d, CLEAN, a, out_ok);

		machine.addTransition(e, WATER, f, out_error);
		machine.addTransition(e, POD, f, out_error);
		machine.addTransition(e, BUTTON, f, out_error);
		machine.addTransition(e, CLEAN, a, out_ok);

		machine.addTransition(f, WATER, f, out_error);
		machine.addTransition(f, POD, f, out_error);
		machine.addTransition(f, BUTTON, f, out_error);
		machine.addTransition(f, CLEAN, f, out_error);


		return machine;
	 }

	static public CompactMealy<String,String> fluentBuildCoffeeMachine(){

		String WATER = "WATER";
		String POD = "POD";
		String BUTTON = "BUTTON";
		String CLEAN = "CLEAN";

		String out_ok = "ok";
		String out_error = "error";
		String out_coffee = "coffee";

		Alphabet<String> inputAlphabet = Alphabets.fromArray(WATER,POD,BUTTON,CLEAN);

		CompactMealy<String, String> fluentCoffeeMachine = AutomatonBuilders.<String, String> newMealy(inputAlphabet)
	    .withInitial("a")
		.from("a")
			.on(WATER).withOutput(out_ok).to("c")
			.on(POD).withOutput(out_ok).to("b")
			.on(BUTTON).withOutput(out_error).to("f")
			.on(CLEAN).withOutput(out_ok).loop()
		.from("b")
			.on(WATER).withOutput(out_ok).to("d")
			.on(POD).withOutput(out_ok).loop()
			.on(BUTTON).withOutput(out_error).to("f")
			.on(CLEAN).withOutput(out_ok).to("a")
		.from("c")
			.on(WATER).withOutput(out_ok).loop()
			.on(POD).withOutput(out_ok).to("d")
			.on(BUTTON).withOutput(out_error).to("f")
			.on(CLEAN).withOutput(out_ok).to("a")
		.from("d")
			.on(WATER, POD).withOutput(out_ok).loop()
			.on(BUTTON).withOutput(out_coffee).to("e")
			.on(CLEAN).withOutput(out_ok).to("a")
		.from("e")
			.on(WATER, POD, BUTTON).withOutput(out_error).to("f")
			.on(CLEAN).withOutput(out_ok).to("a")
		.from("f")
			.on(WATER, POD, BUTTON, CLEAN).withOutput(out_error).loop()
	    .create();


		return fluentCoffeeMachine;
	 }



}
